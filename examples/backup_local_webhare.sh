#!/bin/sh

# This script is assuming 'wh' is available and that we will be able to
# 'ssh' to ourselves using public/private key authentication

# We will backup to $HOME/webhare-backup

export RWH_REMOTE_WEBHARE_DATAROOT="`wh dirs|grep ^WEBHARE_DATAROOT|cut -b 16-`"

if [ -z "$RWH_REMOTE_WEBHARE_DATAROOT" ]; then
  echo "Unable to find the location of your local WebHare"
fi

cd `dirname $0`
export RWH_BACKUPTO=$HOME/webhare-backup
mkdir -p $RWH_BACKUPTO

export RWH_SSHADDR=127.0.0.1
export RWH_VERBOSE=1

exec ../scripts/rsync_webhare_backup.sh
