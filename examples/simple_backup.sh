#!/bin/sh

# This scripts backs up WebHare on server '$1' to directory '$2', including
# any modules

# Reset directory to our script, so we can reach the rsync script
cd `dirname $0`

if [ -z "$2" ]; then
  echo "Syntax: simple_backup.sh <server address> <directory>"
  exit 1
fi

mkdir -p $2 2>/dev/null
if [ ! -d "$2" ]; then
  echo "Not a directory: $2"
  exit 1
fi

export RWH_BACKUPTO="$2"
export RWH_SSHADDR="$1"
export RWH_MODULES=1

# Make sure we only run one instance at a time
exec flock -n "$2/lock" ../scripts/rsync_webhare_backup.sh
