#!/bin/sh

# This scripts resets the backup in directory '$1'

if [ -z "$1" ]; then
  echo "Syntax: reset_backup.sh <directory>"
  exit 1
fi
mkdir -p $1 2>/dev/null
if [ ! -d "$1" ]; then
  echo "Not a directory: $1"
  exit 1
fi
if [ ! -f "$1/lastcompletebackup" ]; then
  echo "No backup ever completed in directory: $1"
  exit 1
fi
if [ "`cat "$1/currentbackup" 2>/dev/null`" != "`cat "$1/lastcompletebackup"`" ]; then
  echo "WARNING! Previous backup did not complete in directory: $1"
fi

rm -f -- "$1/currentbackup"
# all good
exit 0
