#!/bin/sh

# This scripts checks whether the backup in directory '$1' actually finished

if [ -z "$1" ]; then
  echo "Syntax: check_backup_finished.sh <directory>"
  exit 1
fi
if [ ! -d "$1" ]; then
  echo "Not a directory: $1"
  exit 1
fi
if [ ! -f "$1/lastcompletebackup" ]; then
  echo "No backup ever completed in directory: $1"
  exit 1
fi
if [ "`cat $1/currentbackup 2>/dev/null`" != "`cat $1/lastcompletebackup`" ]; then
  echo "Backup did not complete in directory: $1"
  exit 1
fi

# all good
exit 0
