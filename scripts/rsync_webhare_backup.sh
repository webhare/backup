#!/bin/bash
BASEDIR=`dirname $0`

#
# rsync_webhare_backup.sh - take a snapshot of a remote WebHare's WHDB database
#
# All parameters are passed using environment variables.
#
# RWH_BACKUPTO: The folder where database snapshots will be stored (required)
# RWH_MODULES: Set to '1' to get a backup of the installedmodules too
# RWH_SSHADDR: username@machine from which to get the backup (required)
# RWH_SSHOPTS: Additional options to pass to SSH (eg -o StrictHostKeyChecking=no)
# RWH_RSYNCOPTS:  Additional options to pass to rsync (eg -z)
# RWH_REMOTE_WEBHARE_DATAROOT: The location of the WebHare installation (default: /opt/webhare)
# RWH_REMOTE_TEMPDIR: Where to store a temporary version of the backup files (default: /tmp/webharebackup)
# RWH_VERBOSE: Set to '1' for more information during backups
# RWH_DRYRUN: Run all commands, but don't actually transfer any files
# RWH_DEVMACHINE: Development machine, don't error out if webhare isn't running
#

if [ -z "$RWH_BACKUPTO" ]; then
  echo "Missing RWH_BACKUPTO parameter. It should point to the folder where backup subdirectories will be stored"
  exit 1
fi
if [ -z "$RWH_SSHADDR" ]; then
  echo "Missing RWH_SSHADDR parameter. It should be of the form user@machine"
  exit 1
fi

function test_need_backup() # backupfolder
{
  if [ ! -f "$1/currentbackup" ]; then
    mkdir -p "$1"
    echo `date +%Y-%m-%dT%H.%M.%S` > "$1/currentbackup"
  fi
  export CURRENTBACKUP="`cat \"$1/currentbackup\"`"
  if [ "$CURRENTBACKUP" == "`cat \"$1/lastcompletebackup\" 2>/dev/null`" ]; then
    if [ "$RWH_VERBOSE" == "1" ]; then
      echo "Not running, currentbackup == lastcompletebackup. 'rm' \"$1/currentbackup\" to force a new backup"
    fi
    return 1
  fi
  return 0
}

if ! test_need_backup "$RWH_BACKUPTO" ; then
  exit 0
fi
if [ -z "$RWH_REMOTE_WEBHAREDIR" ]; then
  export RWH_REMOTE_WEBHAREDIR="/opt/wh/whtree/"
fi
if [ -z "$RWH_REMOTE_WEBHARE_DATAROOT" ]; then
  export RWH_REMOTE_WEBHARE_DATAROOT="/opt/whdata/"
fi
if [ -z "$RWH_REMOTE_TEMPDIR" ]; then
  export RWH_REMOTE_TEMPDIR="/tmp/webharebackup/"
fi
if [ -z "$RWH_REMOTE_MODULEDIR" ]; then
  export RWH_REMOTE_MODULEDIR="$RWH_REMOTE_WEBHAREDIR/installedmodules"
fi


# Are we dealing with WHDB or PSQL?
SSHCMD="ssh $RWH_SSHOPTS \"$RWH_SSHADDR\" $RWH_CMDPREFIX \"test -d $RWH_REMOTE_WEBHARE_DATAROOT/postgresql\""

[ "$RWH_VERBOSE" == "1" ] && echo ">>>> $SSHCMD"
eval $SSHCMD > /dev/null
RETVAL=$?

if [ "$RETVAL" == "0" ]; then
  [ "$RWH_VERBOSE" == "1" ] && echo "Remote database: PostgreSQL"
  exec $BASEDIR/libexec/do_postgresql_backup.sh
else
  [ "$RWH_VERBOSE" == "1" ] && echo "Remote database: WHDB"
  exec $BASEDIR/libexec/do_whdb_backup.sh
fi
