#!/bin/bash

if [ -z "$RWH_REMOTE_CONTAINER_ROOT" ]; then
  RWH_REMOTE_CONTAINER_ROOT="/"
fi
if [ -z "$RWH_REMOTE_DBASE_BLOBPARENTDIR" ]; then
  RWH_REMOTE_DBASE_BLOBPARENTDIR="$RWH_REMOTE_WEBHARE_DATAROOT/dbase"
fi

function run_rsync()
{
  local CMD START RETVAL

  START="`date`"
  CMD="rsync $@"
  [ "$RWH_VERBOSE" == "1" ] && echo ">>>> $CMD" 1>&2
  eval $CMD
  RETVAL=$?
  if [ "$RETVAL" != "0" -a "$RETVAL" != "24" ]; then #accept 0 (success) or 24 (partial transfer)
    echo "Rsync failure: return code $RETVAL from rsync started at $START"
    echo "Command line was: $CMD"
    return 1
  fi
  return 0
}

function run_ssh()
{
  local CMD START RETVAL

  START="`date`"
  CMD="ssh $@"
  [ "$RWH_VERBOSE" == "1" ] && echo ">>>> $CMD" 1>&2
  eval $CMD
  RETVAL=$?
  return $RETVAL
}


function control_c()
{
  echo "SIGINT"
  kill %1
  kill %2
  exit 1
}

trap control_c SIGINT

if [ "$RWH_VERBOSE" == "1" ]; then
  echo "Starting rsync database backup for $MACH:"
  echo " WebHare dir: $RWH_REMOTE_WEBHAREDIR"
  echo " Temp dir: $RWH_REMOTE_TEMPDIR"
  echo " Database blob parent dir: $RWH_REMOTE_DBASE_BLOBPARENTDIR"
  if [ "$RWH_MODULES" == "1" ]; then
    echo " Modules dir: $RWH_REMOTE_MODULEDIR"
  fi
fi

# Check if dbserver is actually running
SSHCMD="ssh $RWH_SSHOPTS \"$RWH_SSHADDR\" $RWH_CMDPREFIX \"pgrep dbserver\""

[ "$RWH_VERBOSE" == "1" ] && echo ">>>> $SSHCMD"
eval $SSHCMD > /dev/null
RETVAL=$?
if [ "$RETVAL" != "0" ]; then
  if [ "$RWH_VERBOSE" == "1" ]; then
    echo "No running database server found"
  fi
  exit 1
fi

# Start job control, needed to start backup in background
set -m

BACKUPDEST="$RWH_BACKUPTO/$CURRENTBACKUP"
RSYNCOPTS="-zrltWD --chmod=u=rwX,go=rX --timeout=120 $RWH_RSYNCOPTS" #-W rationale: reduce CPU/disk io (blobs won't change)
#don't compress, dbase files are already compressed and blobs are small or large and mostly uncompressable
if [ "$RWH_DRYRUN" == "1" ]; then
  RSYNCOPTS="$RSYNCOPTS -n"
fi
if [ "$RWH_VERBOSE" == "1" ]; then
  RSYNCOPTS="$RSYNCOPTS -v -P"
fi

RSYNCOPTS="$RSYNCOPTS -e \"ssh $RWH_SSHOPTS\""
RSYNCADDR="$RWH_SSHADDR"

BLOB_RSYNCOPTS="$RSYNCOPTS"
MODULES_RSYNCOPTS="$RSYNCOPTS"
LASTCOMPLETEBACKUP=

# A succesfull previous backup is present? Use its blob storage as template
if [ -f "$RWH_BACKUPTO/lastcompletebackup" ]; then
  LASTBACKUP=`cat "$RWH_BACKUPTO/lastcompletebackup"`
  BLOB_RSYNCOPTS="$BLOB_RSYNCOPTS --link-dest \"$RWH_BACKUPTO/$LASTBACKUP/\""
  MODULES_RSYNCOPTS="$MODULES_RSYNCOPTS --link-dest \"$RWH_BACKUPTO/$LASTBACKUP/modules/\""
fi

BACKUP_TEMPBASEDIR="$RWH_REMOTE_TEMPDIR/wh-backup"
BACKUP_TEMPDIR="$BACKUP_TEMPBASEDIR/$$"
if [ -n "$RWH_REMOTE_TEMPDIR_SYNC" ]; then
  BACKUP_TEMPDIR_RSYNC="$RWH_REMOTE_TEMPDIR_SYNC/wh-backup/$$"
else
  BACKUP_TEMPDIR_RSYNC="$RWH_REMOTE_CONTAINER_ROOT/$BACKUP_TEMPDIR"
fi

if [ "$RWH_DRYRUN" != "1" ]; then
  # make temp dir, and accept host key for next rsync
  ssh $RWH_SSHOPTS "$RWH_SSHADDR" "/bin/mkdir -p -- \"$BACKUP_TEMPDIR_RSYNC\""
fi

# Initial sync of blobs
BLOBEXCLUDEOPTS='--exclude="*.whrf" --exclude="*.whdb" --exclude="*.whrfsc" --exclude="*.bak"'
if ! run_rsync "$BLOB_RSYNCOPTS" $BLOBEXCLUDEOPTS "$RSYNCADDR:${RWH_REMOTE_DBASE_BLOBPARENTDIR%/}/" "$BACKUPDEST/" ; then
  echo "Primary sync failed"
  exit 1
fi

BACKUP_REDIR=2>/dev/null
if [ "$RWH_VERBOSE" == "1" ]; then
  BACKUP_REDIR=
fi

if [ "$RWH_DRYRUN" != "1" ]; then
  # clean earlier backups, and make a new one. Remove any existing backup suspend file.
  # Skip host key check on first call
  run_ssh $RWH_SSHOPTS "$RWH_SSHADDR" $RWH_CMDPREFIX "/bin/mkdir -p -- \"$BACKUP_TEMPDIR\""
  run_ssh $RWH_SSHOPTS "$RWH_SSHADDR" $RWH_CMDPREFIX "/bin/rm -f -- \"$BACKUP_TEMPDIR/backup-suspend\""
  run_ssh $RWH_SSHOPTS "$RWH_SSHADDR" $RWH_CMDPREFIX "/bin/rm -- \"$BACKUP_TEMPDIR/backup.\"*" 2>/dev/null

  # start backup in background job
  rm /tmp/backuplog 2> /dev/null

  if run_ssh $RWH_SSHOPTS "$RWH_SSHADDR" $RWH_CMDPREFIX test -f /opt/wh/whtree/doc/CHANGELOG.md ; then #looks like a 4.08
    # all this ENV magic is needed for WH 4.08
    run_ssh $RWH_SSHOPTS "$RWH_SSHADDR" $RWH_CMDPREFIX "env WEBHARE_BASEPORT=13679 WEBHARE_DATAROOT=/opt/whdata WEBHARE_DIR=/opt/wh/whtree \"$RWH_REMOTE_WEBHAREDIR/bin/backup\" -cp --threads --blobmode=reference --suspendfile \"$BACKUP_TEMPDIR/backup-suspend\" \"$BACKUP_TEMPDIR/backup\"" > "$BACKUPDEST/backuplog"  &
  else
    run_ssh $RWH_SSHOPTS "$RWH_SSHADDR" $RWH_CMDPREFIX "\"$RWH_REMOTE_WEBHAREDIR/bin/wh\" exec backup -cp --threads --blobmode=reference --suspendfile \"$BACKUP_TEMPDIR/backup-suspend\" \"$BACKUP_TEMPDIR/backup\"" > "$BACKUPDEST/backuplog"  &
  fi
fi
if [ "$RWH_VERBOSE" == "1" ]; then
  if [ "$RWH_DRYRUN" != "1" ]; then
    tail -n 1000 -f "$BACKUPDEST/backuplog" &
  fi
fi

if [ "$RWH_VERBOSE" == "1" ]; then
  echo "Waiting for suspend file to appear"
fi

while true;
do
  run_ssh $RWH_SSHOPTS "$RWH_SSHADDR" $RWH_CMDPREFIX "test -f \"$BACKUP_TEMPDIR/backup-suspend\""
  RETVAL=$?
  if [ "$RETVAL" == "0" ]; then
    break;
  fi
  run_ssh $RWH_SSHOPTS "$RWH_SSHADDR" $RWH_CMDPREFIX "pgrep backup" > /dev/null
  RETVAL=$?
  if [ "$RETVAL" != "0" ]; then
    echo "Backup process could not be found"
    if [ "$RWH_DRYRUN" != "1" ]; then
      kill %1 # Just for sure, don't want hanging ssh process
      if [ "$RWH_VERBOSE" == "1" ]; then
        kill %2
      fi
    fi
    exit 1
  fi

  if [ "$RWH_VERBOSE" == "1" ]; then
    echo "Does not exist yet, waiting a second"
  fi

  sleep 1
  RETVAL=$?
  if [ "$RETVAL" != "0" ]; then
    # SIGINT just terminates the sleep, but not this script.
    control_c
  fi
done

# sync again
if ! run_rsync "$BLOB_RSYNCOPTS" $BLOBEXCLUDEOPTS "$RSYNCADDR:${RWH_REMOTE_DBASE_BLOBPARENTDIR%/}/" "$BACKUPDEST/" ; then
  echo "Secondary sync for $MACH failed"
  if [ "$RWH_DRYRUN" != "1" ]; then
    kill %1
    kill %2
  fi
  exit 1
fi

# remove the suspend file and wait for the backup to finish
if [ "$RWH_DRYRUN" != "1" ]; then
  if ! run_ssh $RWH_SSHOPTS "$RWH_SSHADDR" $RWH_CMDPREFIX "/bin/rm -- \"$BACKUP_TEMPDIR/backup-suspend\"" ; then
    echo Unable to remove the backup suspend file, assuming failed backup
    exit 1
  fi

  # Bring the backup to the foreground, wait for it to finish
  fg %1 > /dev/null

  # See if backup has really finished
  run_ssh $RWH_SSHOPTS "$RWH_SSHADDR" $RWH_CMDPREFIX "test -f \"$BACKUP_TEMPDIR/backup.md5\""
  RETVAL=$?
  if [ "$RETVAL" != "0" ]; then
    if [ "$RWH_VERBOSE" != "1" ]; then
      cat /tmp/backuplog
    else
      sleep 1
      kill %2
    fi
    echo "Database backup process failed"
    exit 1
  fi
  if [ "$RWH_VERBOSE" == "1" ]; then
    sleep 1
    kill -INT %2
  fi
fi

# sync the database files
if ! run_rsync "$RSYNCOPTS --remove-source-files" "$RSYNCADDR:$BACKUP_TEMPDIR_RSYNC/" "$BACKUPDEST/backup/" ; then
  echo "Database file sync failed"
  exit 1
fi

# verify the database files
cd "$BACKUPDEST/backup"
if which md5sum >/dev/null 2>&1 ; then # we have linux's md5sum
  if ! md5sum --status -c backup.md5 ; then
    echo "MD5 verification failed!"
    exit 1
  fi
elif which md5 >/dev/null 2>&1; then #assuming mac's md5 (perhaps BSD too?)
  ( for P in backup.bk*; do echo "`md5 -q \"$P\"`  $P"; done ) > backup.chk
  if ! diff backup.md5 backup.chk >/dev/null ; then
    echo "MD5 verification failed!"
    exit 1
  fi
else
  echo "Warning - have no way to verify MD5 hashes"
fi

if [ "$RWH_MODULES" == "1" ]; then
  if ! run_rsync "$MODULES_RSYNCOPTS" "$RSYNCADDR:$RWH_REMOTE_MODULEDIR/" "$BACKUPDEST/modules/" ; then
    echo "Modules sync failed"
    exit 1
  fi
fi

if [ "$RWH_NORUN" != "1" ]; then
  run_ssh $RWH_SSHOPTS "$RWH_SSHADDR" $RWH_CMDPREFIX "/bin/rm -rf -- \"$BACKUP_TEMPBASEDIR\""
  if [ "$RWH_DRYRUN" != "1" ]; then
    echo "$CURRENTBACKUP" > "$RWH_BACKUPTO/lastcompletebackup"
  fi
fi
