#!/bin/bash

if [ -z "$RWH_REMOTE_CONTAINER_ROOT" ]; then
  RWH_REMOTE_CONTAINER_ROOT="/"
fi
RWH_REMOTE_DBASEROOT=$RWH_REMOTE_CONTAINER_ROOT/opt/whdata/postgresql/

function run_rsync()
{
  local CMD START RETVAL

  START="`date`"
  CMD="rsync $@"
  [ "$RWH_VERBOSE" == "1" ] && echo ">>>> $CMD" 1>&2
  eval $CMD
  RETVAL=$?
  if [ "$RETVAL" != "0" -a "$RETVAL" != "24" ]; then #accept 0 (success) or 24 (partial transfer)
    echo "Rsync failure: return code $RETVAL from rsync started at $START"
    echo "Command line was: $CMD"
    return 1
  fi
  return 0
}

function run_ssh()
{
  local CMD START RETVAL

  START="`date`"
  CMD="ssh $@"
  [ "$RWH_VERBOSE" == "1" ] && echo ">>>> $CMD" 1>&2
  eval $CMD
  RETVAL=$?
  return $RETVAL
}

if [ "$RWH_VERBOSE" == "1" ]; then
  echo "Starting rsync database backup:"
  echo " WebHare dir: $RWH_REMOTE_WEBHAREDIR"
  echo " Data root: $RWH_REMOTE_WEBHARE_DATAROOT"
  echo " Temp dir: $RWH_REMOTE_TEMPDIR"
  echo " Container root: $RWH_REMOTE_CONTAINER_ROOT"
  if [ "$RWH_MODULES" == "1" ]; then
    echo " Modules dir: $RWH_REMOTE_MODULEDIR"
  fi
fi

[ "$RWH_VERBOSE" == "1" ] && echo ">>>> $SSHCMD"
eval $SSHCMD > /dev/null
RETVAL=$?
if [ "$RETVAL" != "0" ]; then
  if [ "$RWH_VERBOSE" == "1" ]; then
    echo "No running database server found"
  fi
  if [ "$RWH_DEVMACHINE" == "1" ]; then
    exit 2
  fi
  exit 1
fi

BACKUPDEST="$RWH_BACKUPTO/$CURRENTBACKUP"
RSYNCOPTS="-zrltWD --chmod=u=rwX,go=rX --timeout=120 $RWH_RSYNCOPTS" #-W rationale: reduce CPU/disk io (blobs won't change)
#don't compress, dbase files are already compressed and blobs are small or large and mostly uncompressable
if [ "$RWH_DRYRUN" == "1" ]; then
  RSYNCOPTS="$RSYNCOPTS -n"
fi
if [ "$RWH_VERBOSE" == "1" ]; then
  RSYNCOPTS="$RSYNCOPTS -v -P"
fi

RSYNCOPTS="$RSYNCOPTS -e \"ssh $RWH_SSHOPTS\""
RSYNCADDR="$RWH_SSHADDR"

BLOB_RSYNCOPTS="$RSYNCOPTS"
MODULES_RSYNCOPTS="$RSYNCOPTS"
LASTCOMPLETEBACKUP=

# A succesfull previous backup is present? Use its blob storage as template
if [ -f "$RWH_BACKUPTO/lastcompletebackup" ]; then
  LASTBACKUP=`cat "$RWH_BACKUPTO/lastcompletebackup"`
  BLOB_RSYNCOPTS="$BLOB_RSYNCOPTS --link-dest \"$RWH_BACKUPTO/$LASTBACKUP/\""
  MODULES_RSYNCOPTS="$MODULES_RSYNCOPTS --link-dest \"$RWH_BACKUPTO/$LASTBACKUP/modules/\""
fi

BACKUP_TEMPBASEDIR="$RWH_REMOTE_TEMPDIR/wh-backup"
BACKUP_TEMPDIR="$BACKUP_TEMPBASEDIR/$$"

# Initial sync of blobs
if ! run_rsync "$BLOB_RSYNCOPTS" "$RSYNCADDR:${RWH_REMOTE_DBASEROOT%/}/blob" "$BACKUPDEST/" ; then
  echo "Primary sync failed"
  exit 1
fi

# When used with 'nsenter', some machines set an incorrect PATH and it lacks /bin/ (because parent is centos and docker is ubuntu) - this is a workaround for that
if [ "$RWH_DRYRUN" != "1" ]; then
  run_ssh $RWH_SSHOPTS "$RWH_SSHADDR" $RWH_CMDPREFIX "/bin/mkdir -p -- \"$BACKUP_TEMPDIR\""
fi

PGOPTS=""
if [ "$RWH_VERBOSE" == "1" ] ; then
  PGOPTS="-P -v"
fi

run_ssh $RWH_SSHOPTS "$RWH_SSHADDR" $RWH_CMDPREFIX "pg_basebackup -D \"$BACKUP_TEMPDIR/\" -F tar $PGOPTS -h \"$RWH_REMOTE_WEBHARE_DATAROOT/postgresql\" --compress=1"
ERRORCODE="$?"

if [ "$ERRORCODE" != "0" ]; then
  echo "BACKUP FAILED - pg_basebackup step returned error $ERRORCODE"
  exit 1
fi

# sync again
if ! run_rsync "$BLOB_RSYNCOPTS" "$RSYNCADDR:${RWH_REMOTE_DBASEROOT%/}/blob" "$BACKUPDEST/" ; then
  echo "Secondary sync failed"
  exit 1
fi

# sync the database files
if ! run_rsync "$RSYNCOPTS --remove-source-files" "$RSYNCADDR:${RWH_REMOTE_CONTAINER_ROOT%/}/$BACKUP_TEMPDIR/" "$BACKUPDEST/backup/" ; then
  echo "Database file sync failed"
  exit 1
fi

if [ "$RWH_MODULES" == "1" ]; then
  if ! run_rsync "$MODULES_RSYNCOPTS" "$RSYNCADDR:$RWH_REMOTE_MODULEDIR/" "$BACKUPDEST/modules/" ; then
    echo "Modules sync failed"
    exit 1
  fi
fi

if [ "$RWH_NORUN" != "1" ]; then
  run_ssh $RWH_SSHOPTS "$RWH_SSHADDR" $RWH_CMDPREFIX "/bin/rm -rf -- \"$BACKUP_TEMPBASEDIR\""
  if [ "$RWH_DRYRUN" != "1" ]; then
    echo "$CURRENTBACKUP" > "$RWH_BACKUPTO/lastcompletebackup"
  fi
fi
