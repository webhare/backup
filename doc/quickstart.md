# Quickstart

We recommend reading the [full instructions](backup.md) instead of this
quickstart manual to properly customize and tweak the backups. But if you're
in a rush, here are the crontab entries to get started.

We asumme a user named `backupper`, that you have created the directory
`/home/backupper/myserver/`, and stored this repository as `/home/backupper/webhare-backup`

From scratch, on the backup server
```
useradd -m backupper
su - backupper
ssh-keygen
cat ~/.ssh/id_rsa.pub
```

now append the contents of the id_rsa.pub file on the backup server to the
~/.ssh/authorized_keys file on the target webhare account. If this file or
the '.ssh' folder doesn't exist, create it.

Now you should test if you can ssh from the backupper account to the target server.
But before you do, disable the SSH_AUTH_SOCK environment variable to make sure
the ssh agent isn't interfering.

As 'backupper' on 'backup server':
```
unset SSH_AUTH_SOCK
ssh webhare@myserver.com
```

If you haven't checked out this repository yet, now is the time to do so
```
cd /home/backupper/
git clone https://gitlab.com/webhare/backup.git
```

## Preparing
Before using the script, make sure public/private key authentication works
from the server you're installing this on, to the destination server, and that
there will be no authorization prompts.

The best way to do this is to become the user `backupper`, and first disable
any SSH agent - type `unset SSH_AUTH_SOCK` in the shell to make sure you're
testing the private key of user `backupper` and not your current SSH agent setup

Then, manually enter the backup command, eg:
```
/home/backupper/webhare-backup/examples/simple_backup.sh webhare@myserver.com /home/backupper/myserver/
```

## Crontab
Then this should be your crontab. Please note that it's very important to
actually set up a working email address so you'll see notifications when
things get stuck!

```
MAILTO=your@email.com
0 1 * * *         /home/backupper/webhare-backup/examples/reset_backup.sh /home/backupper/myserver/
*/5 2-23 * * *    /home/backupper/webhare-backup/examples/simple_backup.sh webhare@myserver.com /home/backupper/myserver/
```

If you've changed the SSH port on the server to eg 4000, add a line
```
RWH_SSHOPTS="-p 4000"
```
to the top of the crontab file

If all goes fine, you shouldn't receive any email. But don't asumme no news
is good news - it's still a good idea to periodically check whether new backups
are coming in.

Plus, you'll need to regularly clean old backups.
