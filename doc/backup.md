# The backup process

## Setting up backup
The backup script takes its instructions from a set of environment variables.

We recommend wrapping your backup process in a shell script (see the examples/
directory) if you're backing up a single server, or have your backup/management
server work using some sort of inventory to backup all relevant servers.

The backup script requires 'rsync' and 'ssh' to be available, and to be able
to 'ssh' without explicitly specifying credentials or passwords. Usually public/private
key authentication works best.

If the backup is initiated from a remote server (recommended!) it only needs to
be able to ssh into the webhare account of the backed up server.

## Backup settings
The backup process is controlled by the following environment variables:

- RWH_BACKUPTO
The destination directory (required). The destination should be unique for each
backed up WebHare server.

- RWH_MODULES
Set to '1' to get a backup of the installedmodules too

- RWH_SSHADDR
username@machine from which to get the backup (required)

- RWH_SSHOPTS
Additional options to pass to SSH (eg -o StrictHostKeyChecking=no)

- RWH_RSYNCOPTS
Additional options to pass to rsync (eg -z)

- RWH_REMOTE_WEBHAREDIR
The location of the WebHare installation (default: /opt/webhare) - this is the
directory that contains 'bin/backup'

- RWH_REMOTE_DBASE_BLOBDIR
The location of the database blob files (default: /opt/webhare/dbase/blob)

- RWH_REMOTE_MODULEDIR
The location of the WebHare installation's addon modules (default: /opt/webhare/var/installedmodules)

- RWH_REMOTE_TEMPDIR
Where to store a temporary version of the backup files (default: /tmp/webharebackup)

- RWH_VERBOSE
Set to '1' for more information during backups

- RWH_DRYRUN
Set to '1' to show all commands, but don't actually transfer any files

## Running the backup
To properly run the (incrementals) backup, you need to understand the process
used by rsync_webhare_backup.sh to determine whether to start a new backup.

All files and directories mentioned here are relatove to the $RWH_BACKUPTO directory

1) If the file 'currentbackup' does not exist, the script will create one, containing the current date and time

2) If the file 'lastcompletebackup' exists and contains the same date and time as 'currentbackup', the script will assume
   the backup to be complete, and exit.

3) The script will backup to the directory named in the contents of 'currentbackup'. The rsync of blobs will resume
   where it left off, if the script was aborted earlier, and will use the blobs from the directory named in 'lastcompletebackup'
   as a potential source of data (ie, if the blob already exists in the previous backup, it will be hardlinked instead of
   redownloaded. See the rsync '--link-dest' option for more information)

4) If the backup completes, the contents of 'currentbackup' are copied over 'lastcompletebackup'

This implies that, once a backup completes, simply rerunning rsync_webhare_backup.sh will never start a new backup. You
need to remove the 'currentbackup' file to start the next backup.

A daily backup schedule might look like this:
- At 1:00AM, verify that currentbackup and lastcompletebackup have the same contents. If not, warn that the previous backup
  never completed
- At 1:45AM, delete the currentbackup file
- At 2:00AM until midnight, start the backup process every 5 minutes if it wasn't running

examples/reset_backup.sh combines the first two steps of the above schedule

## Verifying the backup
...

## Restoring a backup
The backed up files cannot be used directly...
